<?php
/*
Plugin Name: Custom Shortcodes
Plugin URI: https://bitbucket.org/tareq_mahmood/custom_shortcode_plugin_wp/
Description: Custom Shortcodes for the restaurant name, email, address etc and so on.
Author: Tareq Mahmood
Author URI: http://tareqmahmood.com
Version: 0.9
License: GPLv2
*/
//Enables editor access to necessary functions when plugin is enabled

function shortcode_site_name()
{
    return get_bloginfo('name','display'); //get_option('blogname');
}

function shortcode_restaurant_name()
{
    return get_option('restaurant_name');
}

function shortcode_phone()
{
    return get_option('phone');
}

function shortcode_public_phone()
{
    return get_option('public_phone');
}

function shortcode_admin_email()
{
    return get_option('admin_email');
}

function shortcode_contact_email()
{
    return get_option('contact_email','public@public.com');
}

function shortcode_owner_name()
{
    return get_option('owner_name');
}

function shortcode_copyright_message()
{
    return customize_value(get_option('copyright_message'));
}

function shortcode_address_street()
{
    return get_option('address_street');
}

function shortcode_address_city()
{
    return get_option('address_city');
}

function shortcode_address_prov()
{
    return get_option('address_prov');
}

function shortcode_address_country()
{
    return get_option('address_country');
}

function shortcode_address_zip()
{
    return get_option('address_zip');
}

//Apply Customizations to ShortCode Value
function customize_value($text)
{
    $vars = array(
        '$YYYY' => date('Y'),
    );
    foreach($vars as $var => $val)
    {
        $text = str_replace($var, $val, $text);
    }
    return $text;
}

/* Admin Menu
 * 
 */

if(is_admin()){	
    add_action('admin_menu', 'custom_shortcode_options');
}

function custom_shortcode_options()
{
	add_menu_page('Custom Shortcodes', 'Custom Shortcodes', 'manage_options', 'custom_shortcode_options', 'custom_shortcode_options_page',plugin_dir_url( __FILE__ ) .'/shortcode_icon.jpg',25.23);
	
}

function custom_br2nl($main_text) {
    $breaks = array("<br />","<br>","<br/>");
    $text = str_ireplace($breaks, "\n", $main_text);
    return $text;
}

function custom_nl2br($main_text) {
    return preg_replace('/\n\r?/', "<br />", $main_text);
}

function custom_shortcode_options_page()
{
    $table_name = 'ShortCode';
	$shortCodes = array(
            'website_name' => 'Website Name',
            'restaurant_name' => 'Restaurant Name',
            'admin_email' => 'Admin Email',
            'contact_email' => 'Contact Email',
            'phone' => 'Phone',
            'public_phone' => 'Public Phone',
            'owner_name' => 'Owner Name',
            'copyright_message' => 'Copyright Message',
            'address_street' => 'Street',
            'address_city' => 'City',
            'address_prov' => 'Province',
            'address_country' => 'Country',
            'address_zip' => 'Zip',
        );
	if(isset($_POST[$table_name]) && isset($_POST[$table_name]['Submit']))
	{
		foreach($_POST[$table_name] as $key => $value) {
                    if($key == 'website_name') {
                        update_option( 'blogname', stripslashes(urldecode($value)) );
                    } elseif($key == 'address_street') {
                        update_option($key, custom_nl2br(stripslashes(urldecode($value))) );
                    } elseif($key != 'Submit') {
                        update_option($key, stripslashes(urldecode($value)));
                    }
                }
	}
		
		 ?><div class="wrap" style="font-size:13px;">

			<div class="icon32" id="icon-options-general"><br/></div><h2>Custom ShortCodes</h2>
			<form method="post">
                            <table class="form-table" style="width:70%;">
            <tr>
                <th><b>Name</b></th><th><b>ShortCode</b></th><th><b>Value</b></th></tr>
            <tr><td>Website Name</td><td>website_name</td><td> <input type="text" name="<?php echo $table_name;?>[<?php echo $shortcode;?>]" value="<?php echo get_bloginfo('name','display'); ?>" /></td></td></tr>
            <?php foreach($shortCodes as $shortcode => $name)
                        if($shortcode == 'address_street') { 
                            ?><tr><td><?php echo $name;?></td><td><?php echo $shortcode;?></td><td><textarea name="<?php echo $table_name;?>[<?php echo $shortcode;?>]" ><?php echo custom_br2nl(get_option($shortcode));?></textarea></td></tr>
			<?php 
                        } elseif($shortcode != 'website_name') {
			{
				?><tr><td><?php echo $name;?></td><td><?php echo $shortcode;?></td><td><input type="text" name="<?php echo $table_name;?>[<?php echo $shortcode;?>]" value="<?php echo get_option($shortcode);?>" /></td></tr>
			<?php 
                        
                        }
                  }?>
            </tr>
            <tr><td></td><td></td><td><input type="submit" name="<?php echo $table_name;?>[Submit]" value="Save Changes" /></td></tr>
            </table>
            </form>
            </div>
            <?php 
	
}

add_shortcode('website_name','shortcode_site_name');
add_shortcode('restaurant_name','shortcode_restaurant_name');
add_shortcode('phone','shortcode_phone');
add_shortcode('public_phone','shortcode_public_phone');
add_shortcode('admin_email','shortcode_admin_email');
add_shortcode('contact_email','shortcode_contact_email');
add_shortcode('owner_name','shortcode_owner_name');
add_shortcode('copyright_message','shortcode_copyright_message');
add_shortcode('address_street','shortcode_address_street');
add_shortcode('address_city','shortcode_address_city');
add_shortcode('address_prov','shortcode_address_prov');
add_shortcode('address_country','shortcode_address_country');
add_shortcode('address_zip','shortcode_address_zip');
//for shortcode working in widgets
add_filter('widget_text', 'do_shortcode');